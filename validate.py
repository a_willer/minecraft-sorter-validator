from rcon import Client
import yaml
from nbtlib import parse_nbt
import json
import logging

logger = logging.getLogger(__name__)

class SorterValidator:
    def __init__(self, config):
        self.NUM_CHESTS_PER_SIDE = 12
        self.config = config

    def __get_rcon_client(self):
        return Client(self.config['rcon_host'], self.config['rcon_port'], passwd=self.config['rcon_password'])

    def __transform_to_world(self, coords, offset_coords=(0, 0, 0)):
        orientation = self.config['sorter_position']['orientation']

        transformed = {
            'east': (coords[0], coords[1], coords[2]),
            'south': (-coords[2], coords[1], coords[0]),
            'west': (-coords[0], coords[1], -coords[2]),
            'north': (coords[2], coords[1], -coords[0]),
        }[orientation]

        return (transformed[0] + offset_coords[0], transformed[1] + offset_coords[1], transformed[2] + offset_coords[2])

    def __get_chest_positions(self):
        # Method assumes 'normal' east orientation with first chest in positive X direction from reference point
        base_coords = (self.config['sorter_position']['x'], self.config['sorter_position']['y'], self.config['sorter_position']['z'])
        y_chest = 2
        chests = []

        # Side A
        for offset in range(self.NUM_CHESTS_PER_SIDE):
            first_chest_half_x = 1
            second_chest_half_x = first_chest_half_x + 1
            coords = [ (first_chest_half_x, y_chest, offset), (second_chest_half_x, y_chest, offset) ]
            chests.append(coords)
        # Side B
        for offset in range(self.NUM_CHESTS_PER_SIDE):
            first_chest_half_z = 19
            second_chest_half_z = first_chest_half_z + 1
            chest_x = 1 - 8

            coords = [ (chest_x - offset , y_chest, first_chest_half_z), (chest_x - offset, y_chest, second_chest_half_z) ]
            chests.append(coords)
        # Side C
        for offset in range(self.NUM_CHESTS_PER_SIDE):
            first_chest_half_x = -26
            second_chest_half_x = first_chest_half_x - 1
            coords = [ (first_chest_half_x, y_chest, self.NUM_CHESTS_PER_SIDE - 1 - offset), (second_chest_half_x, y_chest, self.NUM_CHESTS_PER_SIDE - 1 - offset) ]
            chests.append(coords)

        transformed_chests = []
        for coords in chests:
            transformed = [ self.__transform_to_world(coords[0], base_coords), self.__transform_to_world(coords[1], base_coords) ]
            transformed_chests.append(transformed)

        return transformed_chests

    def __get_chest_contents_nbts(self, chest_coords):
        result = []
        with self.__get_rcon_client() as client:
            for chest in chest_coords:
                response_first_half_chest = client.run('data', 'get', 'block', str(chest[0][0]), str(chest[0][1]), str(chest[0][2]))
                response_second_half_chest = client.run('data', 'get', 'block', str(chest[1][0]), str(chest[1][1]), str(chest[1][2]))
                nbt_first_half_chest_items = parse_nbt(response_first_half_chest.split(': ', maxsplit=1)[1])['Items']
                nbt_second_half_chest_items = parse_nbt(response_second_half_chest.split(': ', maxsplit=1)[1])['Items']
                result.append([nbt_first_half_chest_items, nbt_second_half_chest_items])

        return result

    def __transform_chest_contents_nbts(self, chest_contents_nbts):

        def unpack(nbts):
            result = {}

            for chest_index in range(2):
                for item in nbts[chest_index]:
                    item = item.unpack()
                    display_name = json.loads(item.get('tag', {}).get('display', {}).get('Name', '{}')).get('text', '')
                    data = {
                        'id': item['id'],
                        'Count': item['Count'],
                        'display_name': display_name,
                        'tag': item.get('tag', {})
                    }
                    result[item['Slot'] + 27 * chest_index] = data

            return result

        chest_contents = {}

        for chest_index in range(self.NUM_CHESTS_PER_SIDE * 3):
            side = ['A', 'B', 'C'][chest_index // self.NUM_CHESTS_PER_SIDE]
            side_index = chest_index % self.NUM_CHESTS_PER_SIDE
            chest_id = f'{side}{side_index}'
            chest_contents[chest_id] = unpack(chest_contents_nbts[chest_index])

        return chest_contents

    def __validate_chest_contents(self, chest_contents):
        item_data_by_name = { v['name']:v for v in self.item_data }

        result = {}
        for chest_id, contents in chest_contents.items():
            chest_value = 0
            for slot, item in contents.items():
                stack_size = item_data_by_name[item['id'].split(':', maxsplit=1)[1]]['stackSize']
                item_weight = 64 // stack_size
                chest_value += item_weight * item['Count']

                if slot == 0:
                    if item['id'] != 'minecraft:potion' or item['tag'].get('Potion', '') != 'minecraft:water':
                        logger.error(f"Chest {chest_id} does not contain 'minecraft:potion' of type 'minecraft:water' in slot 0.")
                    continue

                if item['display_name'] == 'Filler Item':
                    if item['id'] == 'minecraft:oak_sign':
                        if slot != 1:
                            logger.error(f"Chest {chest_id} contains oak sign filler item in slot {slot} instead of expected slot 1.")
                    elif item['id'] == 'minecraft:stone_button':
                        if slot == 2 and item['Count'] != 64:
                            logger.error(f"Chest {chest_id} does not contain 64 stone button filler items in slot 2.")

                if item['display_name'] == 'Cart Filler' or item['display_name'] == 'Sword aligner':
                    logger.error(f"Chest {chest_id} contains illegal item {item['display_name']} in slot {slot}.")

            if chest_value != 247:
                logger.error(f"Chest {chest_id} does have chest value {chest_value} instead of expected value 247.")

    def __summarize_chest_contents(self, chest_contents):
        result = {}
        for chest_id, contents in chest_contents.items():
            item_summary = {}
            for slot, item in contents.items():
                if item['display_name'] == 'Filler Item':
                    continue

                item_summary[slot] = {
                    'id': item['id'],
                    'display name': item['display_name'],
                    'count': item['Count'],
                }
            result[chest_id] = {
                'items': item_summary,
            }
        
        return result

    def check(self):
        with open(f"./minecraft-data/data/pc/{self.config['minecraft_data_file_version']}/items.json") as f:
            self.item_data = json.load(f)

        chest_coords = self.__get_chest_positions()
        chest_contents_nbts = self.__get_chest_contents_nbts(chest_coords)
        chest_contents = self.__transform_chest_contents_nbts(chest_contents_nbts)
        self.__validate_chest_contents(chest_contents)
        chest_contents_summary = self.__summarize_chest_contents(chest_contents)

        #print(yaml.dump(chest_contents_summary, indent=4, sort_keys=False))


with open('config.yaml') as f:
    config = yaml.load(f, Loader=yaml.FullLoader)

validator = SorterValidator(config)
validator.check()
